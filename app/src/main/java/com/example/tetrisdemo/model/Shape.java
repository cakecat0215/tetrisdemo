package com.example.tetrisdemo.model;

public class Shape {
    public int x, y;
    public BoardCell[][] mat = new BoardCell[5][5];
    public boolean canRotate;

    Shape() {
        for (int i = 0; i < 5; ++i) {
            for (int j = 0; j < 5; ++j) {
                mat[i][j] = new BoardCell();
            }
        }
        x = y = 0;
        canRotate = true;
    }

    Shape(int[][] _mat, int _color) {
        for (int i = 0; i < 5; ++i) {
            for (int j = 0; j < 5; ++j) {
                if (_mat[i][j] == 1) {
                    mat[i][j] = new BoardCell(_mat[i][j], _color);
                } else {
                    mat[i][j] = new BoardCell();
                }
            }
        }
        x = y = 0;
        canRotate = true;
    }

    public Shape(int[][] _mat, int _color, final int behavior) {
        for (int i = 0; i < 5; ++i) {
            for (int j = 0; j < 5; ++j) {
                if (_mat[i][j] == 1)
                    mat[i][j] = new BoardCell(_mat[i][j], _color, behavior);
                else
                    mat[i][j] = new BoardCell();

            }
        }
        canRotate = true;
    }

    public Shape(int[][] _mat, int _color, final int behavior, boolean _canRotate) {
        for (int i = 0; i < 5; ++i) {
            for (int j = 0; j < 5; ++j) {
                if (_mat[i][j] == 1)
                    mat[i][j] = new BoardCell(_mat[i][j], _color, behavior);
                else
                    mat[i][j] = new BoardCell();

            }
        }
        canRotate = _canRotate;
    }

    Shape(int[][] _mat, int _color, int _x, int _y) {
        for (int i = 0; i < 5; ++i) {
            for (int j = 0; j < 5; ++j) {
                if (_mat[i][j] == 1) {
                    mat[i][j] = new BoardCell(_mat[i][j], _color);
                } else {
                    mat[i][j] = new BoardCell();
                }
            }
        }
        x = _x;
        y = _y;
        canRotate = true;
    }


    public void RotateLeft() {
        if (!this.canRotate) {
            return;
        }

        BoardCell[][] aux = new BoardCell[5][5];
        for (int i = 1; i < 5; ++i) {
            for (int j = 1; j < 5; ++j) {
                aux[4 - j + 1][i] = mat[i][j];
            }
        }
        for (int i = 1; i < 5; ++i) {
            for (int j = 1; j < 5; ++j) {
                mat[i][j] = aux[i][j];
            }
        }
    }

    public void RotateRight() {
        if (!this.canRotate) {
            return;
        }

        BoardCell[][] aux = new BoardCell[5][5];
        for (int i = 1; i < 5; ++i) {
            for (int j = 1; j < 5; ++j) {
                aux[j][4 - i + 1] = mat[i][j];
            }
        }
        for (int i = 1; i < 5; ++i) {
            for (int j = 1; j < 5; ++j) {
                mat[i][j] = aux[i][j];
            }
        }
    }
}