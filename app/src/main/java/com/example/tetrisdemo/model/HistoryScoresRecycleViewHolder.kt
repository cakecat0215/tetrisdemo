package com.example.tetrisdemo.model

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.tetrisdemo.R

class HistoryScoresRecycleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val playerName = itemView.findViewById<TextView>(R.id.player_name)
    val playerScores = itemView.findViewById<TextView>(R.id.player_scores)
}