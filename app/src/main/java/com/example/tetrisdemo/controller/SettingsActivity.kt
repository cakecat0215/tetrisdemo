package com.example.tetrisdemo.controller

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.tetrisdemo.R
import kotlinx.android.synthetic.main.activity_settings.*


class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        getDifficultyInfo()
        getSpeedInfo()
    }

    private fun getDifficultyInfo(){
        val spnAdapter = ArrayAdapter.createFromResource(
            this,  //對應的Context
            R.array.difficulty_list,  //選項資料內容
            R.layout.spinner_item
        ) //自訂getView()介面格式(Spinner介面未展開時的View)
        spnAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item) //自訂getDropDownView()介面格式(Spinner介面展開時，View所使用的每個item格式)
        difficulty_setting.let {
            it.adapter = spnAdapter //將宣告好的 Adapter 設定給 Spinner
            val getGameDifficulty = DataManager(this).getGameDifficulty()
            it.setSelection(getGameDifficulty)
        }
    }

    private fun getSpeedInfo(){
        speed_setting.let {
            val getSpeed = DataManager(this).getGameSpeed()
            it.text = getSpeed.toString()
            it.setOnClickListener {
                showSpeedDialog()
            }
        }
    }

    private fun showSpeedDialog(){
        val dilogView = LayoutInflater.from(this).inflate(R.layout.dialog_set_speed, null)
        val editSpeedView : EditText
        editSpeedView = dilogView.findViewById(R.id.set_speed)
        val getSpeed = DataManager(this).getGameSpeed()
        editSpeedView.setText(getSpeed.toString())
        val dialogBuilder = AlertDialog.Builder(this)
        dialogBuilder.setView(dilogView)
        dialogBuilder.setCancelable(false)
        dialogBuilder.setNegativeButton("取消"){ dialog: DialogInterface, id: Int ->
            dialog.dismiss()
        }
        dialogBuilder.setPositiveButton("设定"){ dialog: DialogInterface, id: Int ->
            val editSpeed = editSpeedView.text.toString()
            if (editSpeed.isNotBlank() && editSpeed.isNotEmpty()){
                if (editSpeed.toFloat() > 2f){
                    Toast.makeText(this, "數值不能大於2", Toast.LENGTH_SHORT).show()
                }else if(editSpeed.toFloat() == 0f){
                    Toast.makeText(this, "數值不能為0", Toast.LENGTH_SHORT).show()
                }else{
                    DataManager(this).setGameSpeed(editSpeed.toFloat())
                    speed_setting.text = editSpeed
                }
            }else{
                Toast.makeText(this, "數值異常", Toast.LENGTH_SHORT).show()
            }
            dialog.dismiss()
        }
        val dialog = dialogBuilder.create()
        dialog.show()
    }
}
