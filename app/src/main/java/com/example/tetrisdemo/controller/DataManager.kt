package com.example.tetrisdemo.controller

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager

class DataManager(context: Context) {
    private var ctx =context

    fun setGameSettings(difficulty: Int,speed: Float,rows: Int,columns: Int){
        val settings: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx)
        settings.edit().putInt("困難度",difficulty).apply()
        settings.edit().putFloat("速度權重",speed).apply()
        settings.edit().putInt("行",rows).apply()
        settings.edit().putInt("列",columns).apply()
        settings.edit().putBoolean("初始化",true).apply()
    }

    fun checkInitialization():Boolean{
        val settings: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx)
        return settings.getBoolean("初始化",false)
    }

    fun setGameDifficulty(difficulty:Int){
        val settings: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx)
        settings.edit().putInt("困難度",difficulty).apply()
    }

    fun setGameSpeed(speed:Float){
        val settings: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx)
        settings.edit().putFloat("速度權重",speed).apply()
    }

    fun setGameRows(rows:Int){
        val settings: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx)
        settings.edit().putInt("行",rows).apply()
    }

    fun setGameColumns(columns:Int){
        val settings: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx)
        settings.edit().putInt("列",columns).apply()
    }

    fun getGameDifficulty():Int{
        val settings: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx)
        return settings.getInt("困難度",0)
    }

    fun getGameSpeed():Float{
        val settings: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx)
        return settings.getFloat("速度權重",1f)
    }

    fun getGameRows():Int{
        val settings: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx)
        return settings.getInt("行",25)
    }

    fun getGameColumns():Int{
        val settings: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx)
        return settings.getInt("列",15)
    }

    fun setPlayerSettings(name:String,scores:Int = 0){
        var nameSet = getPlayerName()
        if (nameSet == null){
            nameSet = mutableSetOf(name)
        }else{
            nameSet.add(name)
        }
        setPlayerName(nameSet)
        setPlayerScores(name,scores)
    }

    fun setPlayerName(nameSet:MutableSet<String>){
        val settings: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx)
        settings.edit().putStringSet("姓名",nameSet).apply()
    }

    fun setPlayerScores(name: String,scores:Int){
        val settings: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx)
        val getPlayerName = getPlayerName()
        if (getPlayerName != null && getPlayerName.contains(name)){
            settings.edit().putInt(name,scores).apply()
        }
    }

    fun getPlayerName():MutableSet<String>?{
        val settings: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx)
        return settings.getStringSet("姓名",null)
    }

    fun getPlayerScores(name: String):Int?{
        val settings: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx)
        val getPlayerName = getPlayerName()
        if (getPlayerName != null && getPlayerName.contains(name)){
            return settings.getInt(name,0)
        }else{
            return null
        }
    }

    fun removePlayerDate(name: String){
        val settings: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx)
        val nameSet= getPlayerName()
        if (nameSet == null || !nameSet.contains(name)){
            return
        }
        for (i in nameSet.indices){
            if (nameSet.elementAt(i) == name){
                nameSet.remove(name)
                break
            }
        }
        setPlayerName(nameSet)
        settings.edit().remove(name).apply()
    }
}