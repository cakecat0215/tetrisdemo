package com.example.tetrisdemo.controller

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tetrisdemo.R
import kotlinx.android.synthetic.main.activity_history_scores.*

class HistoryScoresActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history_scores)
        recycler_view.layoutManager = LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false) // 必須設置 LayoutManager
        val recycleAdapter = HistoryScoresRecycleAdapter(this)
        recycler_view.adapter = recycleAdapter
    }
}
