package com.example.tetrisdemo.controller

import android.content.Context
import android.util.AttributeSet

class DifficultySpinner : androidx.appcompat.widget.AppCompatSpinner {
    constructor(context: Context) : super(context) {}
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {}
    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {}

    override fun setSelection(position: Int, animate: Boolean) {
        super.setSelection(position, animate)
        setDifficultyIndex(position)
    }

    override fun setSelection(position: Int) {
        super.setSelection(position)
        setDifficultyIndex(position)
    }

    private fun setDifficultyIndex(position: Int){
        DataManager(this.context).setGameDifficulty(position)
        DataManager(this.context).setGameRows(if (position == 0) 25 else 20)
        DataManager(this.context).setGameColumns(if (position == 0) 15 else 10)
    }
}