package com.example.tetrisdemo.controller

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.tetrisdemo.R
import com.example.tetrisdemo.model.Player
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (!DataManager(this).checkInitialization()){
            DataManager(this).setGameSettings(difficulty = 0,speed = 1f,rows = 25,columns = 15)
        }
        button_new_game.setOnClickListener {
            val name = edit_text_name.text.toString()
            if (name.isNotEmpty()){
                val playerNameSet = DataManager(this).getPlayerName()
                if (playerNameSet == null){
                    DataManager(this).setPlayerSettings(name)
                    Player.name = name
                    this@MainActivity.startActivity(Intent(this@MainActivity, GameActivity::class.java))
                }else{
                    for ( i in playerNameSet.indices){
                        if (playerNameSet.elementAt(i) == name){
                            Toast.makeText(this,"暱稱已重複",Toast.LENGTH_SHORT).show()
                            return@setOnClickListener
                        }
                    }
                    DataManager(this).setPlayerSettings(name)
                    Player.name = name
                    this@MainActivity.startActivity(Intent(this@MainActivity, GameActivity::class.java))
                }
            }else{
                Toast.makeText(this, "請輸入暱稱", Toast.LENGTH_SHORT).show()
            }
        }

        button_history_scores.setOnClickListener{
            this@MainActivity.startActivity(
                Intent(
                    this@MainActivity,
                    HistoryScoresActivity::class.java
                )
            )
        }

        button_settings.setOnClickListener{
            this@MainActivity.startActivity(Intent(this@MainActivity, SettingsActivity::class.java))
        }
    }
}
