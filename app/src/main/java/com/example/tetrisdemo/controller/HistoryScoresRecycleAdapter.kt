package com.example.tetrisdemo.controller

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.tetrisdemo.R
import com.example.tetrisdemo.model.HistoryScoresRecycleViewHolder

class HistoryScoresRecycleAdapter(context: Context): RecyclerView.Adapter<HistoryScoresRecycleViewHolder>() {

    private val ctx = context


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryScoresRecycleViewHolder {
        val view = LayoutInflater.from(ctx).inflate(R.layout.recycle_item,parent,false)
        return HistoryScoresRecycleViewHolder(view)
    }

    override fun onBindViewHolder(holder: HistoryScoresRecycleViewHolder, position: Int) {
        val playerNameSet = DataManager(ctx).getPlayerName()
        if (playerNameSet != null && playerNameSet.isNotEmpty()){

            val playerName = holder.playerName
            val playerScores = holder.playerScores
            val playerNameData =  playerNameSet.elementAt(position)
            playerName.text = playerNameData
            for (i in playerNameSet.indices){
                val name = playerNameSet.elementAt(i)
                val scores = DataManager(ctx).getPlayerScores(name)
                if (scores != null){
                    playerScores.text = scores.toString()
                }
            }

            holder.itemView.setOnClickListener {
                val dialogBuilder = AlertDialog.Builder(ctx)
                dialogBuilder.setTitle("是否刪除?")
                dialogBuilder.setNegativeButton("取消"){ dialogInterface: DialogInterface, i: Int ->
                    dialogInterface.dismiss()
                }
                dialogBuilder.setPositiveButton("確定"){ dialogInterface: DialogInterface, i: Int ->
                    DataManager(ctx).removePlayerDate(playerNameData)
                    notifyDataSetChanged()
                    dialogInterface.dismiss()
                }
                val dialog = dialogBuilder.create()
                dialog.show()
            }
        }
    }

    override fun getItemCount(): Int {
        val playerNameSet = DataManager(ctx).getPlayerName()
        return if (playerNameSet != null && playerNameSet.isNotEmpty()){
            playerNameSet.size
        }else{
            0
        }
    }
}